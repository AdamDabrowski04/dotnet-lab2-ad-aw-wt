﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            Board testBoard = new Board();
            testBoard.print();
            Console.ReadKey();
            for (; ; )
            {
                testBoard.defineFutureState();
                testBoard.updateBoard();
                Console.Clear();
                testBoard.print();
                Console.ReadKey();
            }
        }
    }

    class Board
    {
        private Cell[,] board = new Cell[10, 10];
        public Board()
        {
         for (int j = 0; j < 10; j++)
            {
                for (int i = 0; i < 10; i++)
                {
                    board[j, i]=new Cell();
                    if(i==0||j==0||i==9||j==9)
                    {
                        board[j, i].state = false; 
                    }
                   
                }
            }
}

        public void print()
        {
            for (int j = 0; j < 10; j++)
            {
                for (int i = 0; i < 10; i++)
                {
                    board[j, i].printCell();
                   
                }
                Console.WriteLine("");
            }

        }
        public int numberOfNeigbours(int j, int i)
        {
            int tmpNumb = 0;
            for (int k = j-1; k <= j+1; k++)
            {
                for (int m = i-1; m <= i+1; m++)
                {
                    if(j!=k&&i!=m)
                    {
                        if(board[k,m].returnState())
                        {
                            tmpNumb++;
                        }
                    }

                }
            }
            return tmpNumb;
        }
        public void updateBoard()
        {
            for (int j = 1; j < 9; j++)
            {
                for (int i = 1; i < 9; i++)
                {
                    board[j, i].state = board[j, i].futureState;
                }
            }
        }
        public void defineFutureState()
        {
            int tmpNumb;
            for (int j = 1; j < 9; j++)
            {
                for (int i = 1; i < 9; i++)
                {
                    tmpNumb = numberOfNeigbours(j, i);
                    if(board[j,i].returnState())
                    {
                        if(tmpNumb<2||tmpNumb>3)
                        {
                            board[j, i].futureState = false;
                        }
                        if (tmpNumb == 2 || tmpNumb==3)
                        {
                            board[j, i].futureState = true;
                        }
                    }
                    else
                    {
                        if (tmpNumb == 3)
                        {
                            board[j, i].futureState = true;
                        }
                        else
                        {
                            board[j, i].futureState = false;

                        }
                    }

                }
            }
        }
    }
        public class Cell
        {
            static private Random rnd = new Random();
            public bool state = false;
            public bool futureState=false;
            public Cell()
            {
               
                if (40 < rnd.Next(0, 100))
                {
                    state = true;
                }
                else
                    state = false;
            }
            public bool returnState()
            {
                return state;
            }
            public void printCell()
            {
                if (state == false)
                {
                    Console.Write('.');
                }
                else
                    Console.Write('*');
            }

        }


    }

